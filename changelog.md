# changelog

## 1.12.2

- 沙盒支持文件上传

## 1.12.1

- 修复重启网关路由状态重置BUG
- 优化SpringCloudGateway

## 1.12.0

- admin后台新增角色管理
- 支持nacos作为注册中心

## 1.11.0

- 秘钥管理改造
- 服务端返回sign
- 新增SDK返回sign处理
- 新增沙箱环境

## 1.10.0

- 新增监控日志

## 1.9.0

- 改造限流
- 增强参数绑定

## 1.8.0

- 支持文件上传

## 1.7.2

- 修复微服务参数绑定BUG
- Admin新增vue界面

## 1.7.1

- 支持接口名版本号放在url后面

## 1.7.0

- 可自定义数据节点名称

## 1.6.0

- 新增应用授权

## 1.5.0

- admin新增signType字段
- 修复easyopen接入无法访问BUG

## 1.4.0

- 新增文档分组显示
- 支持easyopen文档注解
- BUG修复

## 1.3.0

- 新增接口限流功能 [doc](http://durcframework.gitee.io/sop/#/files/10092_%E6%8E%A5%E5%8F%A3%E9%99%90%E6%B5%81?t=1555378655699)
- 新增文档整合功能 [doc](http://durcframework.gitee.io/sop/#/files/10041_%E7%BC%96%E5%86%99%E6%96%87%E6%A1%A3?t=1555378655698)
- 新增springmvc项目接入demo

## 1.2.0

- SOP Admin新增用户登录
- 新增基础SDK(Java,C#) [doc](http://durcframework.gitee.io/sop/#/files/10095_SDK%E5%BC%80%E5%8F%91?t=1554693919597)

## 1.1.0

- 新增ISV管理 [doc](http://durcframework.gitee.io/sop/#/files/10085_ISV%E7%AE%A1%E7%90%86?t=1554123435621)
- 新增接口授权 [doc](http://durcframework.gitee.io/sop/#/files/10090_%E8%B7%AF%E7%94%B1%E6%8E%88%E6%9D%83?t=1554123435621)

## 1.0.0

- 第一次发布
