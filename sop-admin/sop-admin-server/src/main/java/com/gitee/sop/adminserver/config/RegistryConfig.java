package com.gitee.sop.adminserver.config;

import com.gitee.sop.registryapi.config.BaseRegistryConfig;
import org.springframework.context.annotation.Configuration;

/**
 * @author tanghc
 */
@Configuration
public class RegistryConfig extends BaseRegistryConfig {

}
