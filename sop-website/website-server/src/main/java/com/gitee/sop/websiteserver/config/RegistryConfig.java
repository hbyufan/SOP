package com.gitee.sop.websiteserver.config;

import com.gitee.sop.registryapi.config.BaseRegistryConfig;
import org.springframework.context.annotation.Configuration;

/**
 * @author tanghc
 */
@Configuration
public class RegistryConfig extends BaseRegistryConfig {

}